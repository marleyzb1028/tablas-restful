const express = require('express');

function list(req, res, next) {
    const n1 = req.params.id;
    const n2 = req.params.id2;
    const suma = Number(n1) + Number(n2);
    res.send(`Suma de => ${n1} + ${n2} = ${suma}`);
}
function create(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const multiplicacion = Number(n1) * Number(n2);
    res.send(`Multiplicación de => ${n1} * ${n2} = ${multiplicacion}`);
}
function replace(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const division = Number(n1) / Number(n2);
    res.send(`División de => ${n1} / ${n2} = ${division}`);
}
function update(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    const potencia = Math.pow(Number(n1), Number(n2));
    res.send(`Potencia de => ${n1} ^ ${n2} = ${potencia}`);
}
function destroy(req, res, next) {
    const n1 = req.params.id;
    const n2 = req.params.id2;
    const resta = Number(n1) - Number(n2);
    res.send(`Resta de => ${n1} - ${n2} = ${resta}`);
}

module.exports = {list,create,replace,update,destroy};