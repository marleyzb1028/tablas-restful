var express = require('express');
var router = express.Router();

const controller = require('../controllers/results');
/* GET users listing. */
router.get('/:id/:id2', controller.list);
router.post('/', controller.create);
router.put('/', controller.replace);
router.patch('/', controller.update);
router.delete('/:id/:id2', controller.destroy);

module.exports = router;
